/**
 * @format
 */

import {AppRegistry} from 'react-native';
// import App from './App';
import myCard from "./src/myCard";
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => myCard);
