import React, { Component } from 'react'
import { Text, View, StyleSheet, TextInput, 
    TouchableOpacity, Image } from 'react-native'



export default class Contacts extends Component {

    state = {
        name: ''
    } 
    handleButton = () => {
        console.log("--> ")
        console.log("===> ", this.state.name)

    } 
    handleChangeText = (text) => {
        // will re-render
        // this.setState({
        //     name: text
        // })
        // not render
       this.state.name = text
    }


  render() {
      console.log("in render....")
      const { containerStyle, textStyle, 
        inputStyle, buttonStyle, 
        imageStyle, buttonTextStyle } = styles
    return (
      <View style={containerStyle}>
        <Text style={textStyle}> Hello World tome </Text>
        
        <TextInput onChangeText={(text) => this.handleChangeText(text)} 
            autoCapitalize={'words'} placeholder={'Input me'} 
            keyboardType={'email-address'} style={inputStyle}  />
        
        <TouchableOpacity onPress={() => this.handleButton()} 
            style={buttonStyle}>
            <Text style={buttonTextStyle}>Click me</Text>
        </TouchableOpacity>
        <Text style={textStyle}>
            Name: {this.state.name}
        </Text>

        <Image style={imageStyle} source={require('../../img/car.jpg')} />

      </View>
    )
  }
}

const styles = StyleSheet.create({
    containerStyle: {
        marginTop: 50
    },
    textStyle: {
        fontSize: 20
    },
    inputStyle: {
        height: 40,
        borderWidth: 1,
        fontSize: 30,
    },
    buttonStyle: {
        backgroundColor: 'lightblue',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20  
    },
    buttonTextStyle: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#fff',
       
    }, 
    imageStyle: {
        width: 50,
        height: 50
    }
})

