import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  FlatList,
} from 'react-native';
import { file } from '@babel/types';

export default class myCard extends Component {

  constructor(props) {
    super(props);
    this.state = {
      calls: [
        {
            id:1,
            name: "Seavmeng Chham",
            phone:"+855-12-34-56",
            address:"Phnom Penh" , 
            image: require('../img/avatar1.png'),
            email:"seavmeng123@gmail.com"
        },
        {
            id:2,  name: "Nara Sovan",
            phone:"+855-12-34-56",
            address:"Bankok" , 
            image:require('../img/avatar6.png'),
            email:"nara123@gmail.com"
        } ,
        {
            id:3,  name: "Saiya Pen" ,
            phone:"+855-12-34-56",
            address:"Taiwan" , 
            image:require('../img/avatar5.png'),
            email:"saiya123@gmail.com"
        } ,
        {
            id:4,  name: "Sreymai Pon" ,
            phone:"+855-12-34-56",
            address:"New York" , 
            image:require('../img/avatar3.png'),
            email:"sreymai123@gmail.com"
        } 
     ]
    };
  }

  renderItem = ({item}) => {
    return (
      <TouchableOpacity>
        <View style={styles.row}> 
          <Image source={item.image}  style={styles.pic} />  
          <View>
            <View style={styles.nameContainer}>
              <Text style={styles.nameTxt} numberOfLines={1} ellipsizeMode="tail">{item.name}</Text>
              
            </View> 
            <View style={styles.msgContainer}>
              <Text style={styles.lblTxt}>Address:</Text>
              <Text style={styles.msgTxt}> {item.address}</Text>
            </View>
            <View style={styles.msgContainer}>
              <Text style={styles.lblTxt}>Phone:</Text>
              <Text style={styles.msgTxt}> {item.phone}</Text>
            </View>
             <View style={styles.msgContainer}>
              <Text style={styles.lblTxt}>Email:</Text>
              <Text style={styles.msgTxt}> {item.email}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    return(
      <View style={{ flex: 1 }} >
        <FlatList 
          extraData={this.state}
          data={this.state.calls}
          keyExtractor = {(item) => {
            return item.id;
          }}
          renderItem={this.renderItem}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#DCDCDC', 
    backgroundColor: '#f2f2f2',
    borderBottomWidth: 2,
    borderTopWidth: 2,
    borderLeftWidth: 2,
    borderRightWidth: 2,
    padding: 10,
    marginTop:10,  
    justifyContent: 'center',
    width:400,
    marginLeft:5
    // alignItems: 'center',
    // backgroundColor: '#DCDCDC',
    
  },
  pic: {
    borderRadius: 10,
    width: 70,
    height: 70,
    borderColor:'#94e804',
    borderBottomWidth: 2,
    borderTopWidth: 2,
    borderLeftWidth: 2,
    borderRightWidth: 2,
  },
  nameContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 250,
  },
  nameTxt: {
    marginLeft: 15,
    fontWeight: '600',
    color: '#222',
    fontSize: 18,
    width:170,
  },
  mblTxt: {
    fontWeight: '200',
    color: '#777',
    fontSize: 13,
  },
  msgContainer: {
    flexDirection: 'row',
    alignItems: 'center'  
  },
  msgTxt: {
    fontWeight: '400',
    color: '#008B8B',
    fontSize: 12
  },
  lblTxt: { 
    marginLeft: 15
  },
});  